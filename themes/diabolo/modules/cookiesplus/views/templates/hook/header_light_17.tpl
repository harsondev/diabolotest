{*
 * NOTICE OF LICENSE
 *
 * This product is licensed for one customer to use on one installation (test stores and multishop included).
 * Site developer has the right to modify this module to suit their needs, but can not redistribute the module in
 * whole or in part. Any other use of this module constitues a violation of the user agreement.
 *
 * DISCLAIMER
 *
 * NO WARRANTIES OF DATA SAFETY OR MODULE SECURITY
 * ARE EXPRESSED OR IMPLIED. USE THIS MODULE IN ACCORDANCE
 * WITH YOUR MERCHANT AGREEMENT, KNOWING THAT VIOLATIONS OF
 * PCI COMPLIANCY OR A DATA BREACH CAN COST THOUSANDS OF DOLLARS
 * IN FINES AND DAMAGE A STORES REPUTATION. USE AT YOUR OWN RISK.
 *
 *  @author    idnovate.com <info@idnovate.com>
 *  @copyright 2020 idnovate.com
 *  @license   See above
*}

<style>
	.modal { display: none; }
    .cookiesplus-modal .cp-policy {
        display: block;
        clear: both;
        margin: 10px 0;
    }

    .cookiesplus-modal .modal-body {
        overflow-y: auto;
        max-height: 60vh;
        display: flex;
        padding: 5px 15px;
    }

    .cookiesplus-modal .modal-body p {
        margin: 0;
    }

    .cookiesplus-modal .cookie-actions {
        flex: 0 0 auto;
    }

    .header_backdrop.in {
        opacity: {$C_P_OVERLAY_OPACITY|escape:'htmlall':'UTF-8'} !important;
    }

    {if $C_P_BACKGROUND_COLOR}
    .cookiesplus-modal,
    .cookiesplus-modal div {
        background-color: {$C_P_BACKGROUND_COLOR|escape:'htmlall':'UTF-8'} !important;
    }
    {/if}

    {if $C_P_FONT_COLOR}
    .cookiesplus-modal,
    .cookiesplus-modal div,
    .cookiesplus-modal p {
        color: {$C_P_FONT_COLOR|escape:'htmlall':'UTF-8'};
    }
    {/if}

    @media (min-width: 768px) {
        .cookiesplus-modal .modal {
            text-align: center;
        }
        /*.cookiesplus-modal .modal:before {
            content: '';
            height: 100%;
            width: 1px;
            display: inline-block;
            vertical-align: middle;
        }*/

        .cookiesplus-modal .modal .modal-dialog {
            text-align: left;
            margin: 10px auto;
            display: inline-block;
            vertical-align: middle;
            -webkit-transform: none;
            -o-transform: none;
            transform: none;
        }
    }

    @media (max-width: 575px) {
        .cookiesplus-modal .pull-left,
        .cookiesplus-modal .pull-right {
            float: none;
        }

        .cookiesplus-modal .cookie-actions > .pull-left {
            margin-top: 10px;
        }

        .cookiesplus-modal .cp-policy {
            text-align: center;
            display: block;
        }

        .cookiesplus-modal .header_footer {
            text-align: center;
        }
    }

    {if isset($C_P_POSITION) && ($C_P_POSITION == 2 || $C_P_POSITION == 3)}
        .cookiesplus-modal .modal {
            position: fixed;
            top: auto;
            right: auto;
            left: 0;
            bottom: 0;
            height: auto;
            overflow: hidden;
            width: 100%;
        }

        .cookiesplus-modal .modal .modal-dialog {
            max-width: 100%;
            width: 100%;
            margin: 0;
        }

        .cookiesplus-modal .modal .modal-content {
            border-radius: 0;
            min-height: auto
        }

        .cookiesplus-modal .modal .modal-body .content {
            width: 100%;
        }
    {/if}

    {if isset($C_P_OVERLAY) && $C_P_OVERLAY == '0'}
        .header_backdrop { display: none !important; }
        .header_open { overflow: auto; }
    {/if}

    {if isset($C_P_CSS) && $C_P_CSS}
        {$C_P_CSS nofilter}
    {/if}
</style>

<div class="cookiesplus-modal">
    {if isset($C_P_TEXT_BASIC) && $C_P_TEXT_BASIC}
        <div class="modal" id="cookiesplus-bas" style="padding: 0">
            <div class="modal-dialog header_lg">
                <div class="modal-content">
                    <form method="POST" name="cookies">
                        <div class="modal-body">
                            <div class="content">
                                {$C_P_TEXT_BASIC nofilter}
                                {if isset($C_P_MODE) && $C_P_MODE == '1'}
                                    <input type="button" onclick="cookieGdpr.displayModalAdvanced(); return false;" class="cp-more-information cookie-button" value="{l s='Personnalisation' mod='cookiesplus'}" />
                                {else if isset($C_P_MODE) && $C_P_MODE == '2' && isset($C_P_CMS_PAGE) && $C_P_CMS_PAGE}
                                    <a href="{$link->getCMSLink($C_P_CMS_PAGE)|escape:'htmlall':'UTF-8'}" class="pull-right cp-policy" target="_blank">{l s='Privacy & Cookie Policy' mod='cookiesplus'}</a>
                                {/if}
                            </div>
                            <div class="cookie-actions">
                                <input type="submit" name="save-basic" onclick="return cookieGdpr.saveBasic();" class="cp-accept cookie-button" value="{l s='Yes, I accept' mod='cookiesplus'}" />
                                {if isset($C_P_MODE) && $C_P_MODE == '1' && isset($C_P_CMS_PAGE) && $C_P_CMS_PAGE}
                                    <a href="{$link->getCMSLink($C_P_CMS_PAGE)|escape:'htmlall':'UTF-8'}" class="pull-right cp-policy" target="_blank">{l s='Privacy & Cookie Policy' mod='cookiesplus'}</a>
                                {/if}
                                <button type="submit" name="save" onclick="if (cookieGdpr.saveReject()) return;" class="cookiesplus-btn cp-reject cookie-button">{l s='Refuser tout' mod='cookiesplus'}</button>                                
                            </div>
                            <div class="clear"></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    {/if}

    <div class="modal" id="cookiesplus-advanced" style="padding: 0">
        <div class="modal-dialog header_lg">
            <div class="modal-content">
                <form method="POST" name="cookies" id="cookiesplus-form">
                    <div class="header_header">
                        <span class="h1">{l s='Your cookie settings' mod='cookiesplus'}</span>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <div>
                            {if isset($C_P_TEXT_REQUIRED) && $C_P_TEXT_REQUIRED}
                                <div class="card card-block">
                                    <div>{$C_P_TEXT_REQUIRED nofilter}</div>
                                    <div>
                                        <strong>{l s='Accept strictly necessary cookies?' mod='cookiesplus'}</strong>
                                        <div class="form-check">
                                            <input type="checkbox" class="filled-in form-check-input not_uniform comparator" name="essential" id="essential" checked="checked" disabled>
                                            <label class="form-check-label" for="essential">{l s='Yes' mod='cookiesplus'}</label>
                                        </div>
                                    </div>
                                </div>
                            {/if}

                            {if isset($C_P_TEXT_3RDPARTY) && $C_P_TEXT_3RDPARTY}
                                <div class="card card-block">
                                    <div>{$C_P_TEXT_3RDPARTY nofilter}</div>
                                    <div>
                                        <strong>{l s='Accept third-party cookies?' mod='cookiesplus'}</strong>
                                        <div class="form-check">
                                            <input type="checkbox" class="filled-in form-check-input not_uniform comparator" name="thirdparty" id="thirdparty">
                                            <label class="form-check-label" for="thirdparty">{l s='Yes' mod='cookiesplus'}</label>
                                        </div>
                                    </div>
                                </div>
                            {/if}
                        </div>
                    </div>
                    <div class="header_footer">
                        <div class="cookie-actions">
                            <div class="pull-right">
                                <input type="submit" name="save" onclick="return cookieGdpr.save();" class="btn btn-primary pull-right cp-more-information" value="{l s='Save preferences' mod='cookiesplus'}" />
                                {if isset($C_P_CMS_PAGE_ADV) && $C_P_CMS_PAGE_ADV}
                                    <a href="{$link->getCMSLink($C_P_CMS_PAGE_ADV)|escape:'htmlall':'UTF-8'}" class="pull-right cp-policy" target="_blank">{l s='Privacy & Cookie Policy' mod='cookiesplus'}</a>
                                {/if}
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
    }

    {if isset($C_P_JS) && $C_P_JS}
        {$C_P_JS nofilter}
    {/if}
</script>
