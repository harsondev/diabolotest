{assign var=_counter value=0}
{function name="menu" nodes=[] depth=0 parent=null}
    {if $nodes|count}
      <ul class="top-menu" {if $depth == 0}id="top-menu"{/if} data-depth="{$depth}" data-count="{$nodes|@count}">
        {foreach from=$nodes item=node}
            <li class="{$node.type}{if $node.current} current {/if}" id="{$node.page_identifier}">
              {if $depth == 1}<div class="submenu-bloc">{/if}
              {assign var=_counter value=$_counter+1}
                <a
                  class="{if $depth >= 0}dropdown-item{/if}{if $depth === 1} dropdown-submenu{/if}"
                  href="{$node.url}" data-depth="{$depth}"
                  {if $node.open_in_new_window} target="_blank" {/if}
                >                
                  {if $node.children|count}
                    {* Cannot use page identifier as we can have the same page several times *}
                    {assign var=_expand_id value=10|mt_rand:100000}
                    <span data-target="#top_sub_menu_{$_expand_id}" data-toggle="collapse" class="navbar-toggler collapse-icons">
                      <i class="psticon-arrow-down"></i>
                    </span>
                  {/if}
                  <span>{$node.label}</span>
                </a>
                {if $node.children|count}
                <div {if $depth === 0} class="popover sub-menu js-sub-menu collapse"{else} class="collapse"{/if} id="top_sub_menu_{$_expand_id}">
                  {menu nodes=$node.children depth=$node.depth parent=$node}
                </div>
                {/if}
                {if $depth == 1}</div>{/if}
            </li>
        {/foreach}
      </ul>
    {/if}
{/function}

<div class="menu js-top-menu position-static hidden-sm-down" id="_desktop_top_menu">
    {menu nodes=$menu.children}
    <div class="clearfix"></div>
    <ul class="account-menu top-menu" data-depth="0" id="accoutMenu">
      <li>
        <a>
          <span data-target="#account_sub_menu" data-toggle="collapse" class="navbar-toggler collapse-icons">
            <i class="psticon-arrow-down"></i>
          </span>
          <span class="link-label"><i class="psticon-user"></i><span>{l s='Mon compte' d='Shop.Theme.Global'}</span></span>
        </a>
        <div class="popover sub-menu js-sub-menu collapse" id="account_sub_menu">
          <ul>
            <li><a href="{$urls.pages.my_account}">{l s='Mon compte' d='Shop.Theme.Global'}</a></li>
            <li><a href="{$link->getPageLink('contact')}">{l s='Nous contacter' d='Shop.Theme.Global'}</a></li>
            <li><a href="{$link->getCMSLink(6)}">{l s='Mon devis' d='Shop.Theme.Global'}</a></li>
          </ul>
        </div>
      </li>
    </ul>
</div>
