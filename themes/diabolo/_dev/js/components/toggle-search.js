import $ from 'jquery';

export default class ToggleSearch {
  init() {
    const self = this;
    $('.search-toggle').on('click', () => {
      self.toggleSearchbar();
    });
  }
  
  toggleSearchbar() {
    $('#search_widget').toggleClass('open');
  }
}
